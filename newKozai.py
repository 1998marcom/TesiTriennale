#!/usr/bin/env python3
from numpy import *
import matplotlib.pyplot as plt
import sys
from scipy.integrate import solve_ivp

Jz = 0.6

ee0 = linspace(0.5,0.7,10)
i0 = 42*pi/180
w0 = 90*pi/180

fig=plt.figure()
ax1 = fig.add_subplot(111)
#ax2 = fig.add_subplot(212)

for e0 in ee0:
	i0 = arccos(Jz/sqrt(1-e0**2))
	def system(t, y):
		e = y[0]; i = y[1]; w = y[2] 
		et = 1. * 15*pi/2*e*sqrt(1-e**2)*sin(i)**2*sin(w)*cos(w)
		wt = 1. * 3*pi/2/sqrt(1-e**2)*(5*cos(i)**2*sin(w)**2+(1-e**2)*(5*cos(w)**2-3))
		it = -15*pi/2 *e**2/sqrt(1-e**2)*sin(i)*cos(i)*sin(w)*cos(w)
		return [et, it, wt]


	init = [e0, i0, w0]

	FinalT = 10
	result = solve_ivp(system, (0.0, FinalT), init, max_step=FinalT/1000, method='Radau')
	tt = result.t
	yy = result.y
	e, i, w = yy[0], yy[1], yy[2]


	#ax1.plot(w, e, label='eccentricity')
	#ax1.plot(w, i, label='$\iota$')
	#ax1.plot(w, sqrt(1-e**2)*cos(i), label='$\sqrt{1-e^2}\cos(\iota)$')
	ax1.plot(w, cos(i)**2/(1-e**2), label='$\cos^2(\iota)/(1-e^2)$')
	ax1.plot(linspace(0,10,100), array(100*[3/5]), label='crit')
	#plt.ylim(-4,+4)
	#ax1.legend(loc='upper left', fontsize=12)

	#ax1.spines['top'].set_color('none')
	#ax1.spines['bottom'].set_color('none')
	#ax1.spines['left'].set_color('none')
	#ax1.spines['right'].set_color('none')
	ax1.tick_params(labelcolor='k', top=False, bottom=True, left=True, right=False, labelbottom=True)
	ax1.tick_params(axis='y', which='major', labelsize=12)
	ax1.tick_params(axis='x', which='major', labelsize=12)
	ax1.set_ylabel('$\chi(\iota,e)=\cos^2(\iota)/(1-e^2)$', fontsize=12)
	ax1.set_xlabel('$\omega$ [rad]', fontsize=12)
	ax1.set_xlim(-0,+10)
	
	
	#ax2.plot(tt, w, label='$\omega$')
	#ax2.legend(loc='upper left', fontsize=12)
	#ax2.set_ylabel('$\omega$ [rad]', fontsize=12)
	#ax2.set_xlabel('time [$P/\lambda$]', fontsize=12)
	#ax2.tick_params(axis='x', which='major', labelsize=12)
	#ax2.tick_params(axis='y', which='major', labelsize=12)
	#ax2.set_xlim(-0,+10)

	fig.set_size_inches(6, 3)
	#plt.text(0.97, 0.02,'''
	#$e_0$ = 0.25
	#$\iota_0$ = 42°
	#$\omega_0$ = 90°
	#''',
	#	 horizontalalignment='right',
	#	 verticalalignment='bottom',
	#	 fontsize=12,
	#	 transform = plt.gca().transAxes)

#fig.suptitle('Evoluzione di un sistema con $\cos(\iota)\sqrt{1-e^2}=3/5$')
fig.tight_layout()
plt.savefig('presentazione/kozai/newKozai2.png', transparent=True, dpi=300)
plt.show()
