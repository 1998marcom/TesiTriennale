\documentclass[a4paper, 11pt]{article}
\usepackage[
type={CC},
modifier={by-sa},
version={4.0},
]{doclicense}
\usepackage{header}

%\RenewEnviron{figure}{} %Decommentare per rimuovere tutte le figure
%\RenewEnviron{minted}{} %Decommentare per rimuovere tutti i codici

\DeclareMathOperator{\sinc}{sinc}

%\setcounter{chapter}{-1}

\setcounter{tocdepth}{1}
\setcounter{secnumdepth}{2}

\title{Il meccanismo di Lidov-Kozai nel sistema a tre corpi gerarchico \\ {\Large Aspetti analitici e numerici}}%\footnote{\vspace{3pt}\tiny Sorgenti e pdf su \url{https://gitlab.com/1998marcom/TesiTriennale}}}}
\author{Marco Malandrone}
\date{Luglio 2020}

\newcommand\blfootnote[1]{%
	\begingroup
	\renewcommand\thefootnote{}\footnote{#1}%
	\addtocounter{footnote}{-1}%
	\endgroup
}
\pagenumbering{gobble}
\begin{document}
	
	
	
	%All'inizio di ogni sezione mette la tavola dei contenuti
	%\AtBeginSection[]
	%{
	%  \begin{frame}
	%    \frametitle{Table of Contents}
	%    \tableofcontents[currentsection]
	%  \end{frame}
	%}
	
	%\def\nologo{}
	
	
	
	\maketitle
	
	Buongiorno a tutti,
	
	%=============================================================
	\section{Introduzione}
	
	L'argomento che ho scelto per questa presentazione finale è il meccanismo di Lidov-Kozai nel caso di un sistema gerarchico di tre corpi legati gravitazionalmente.
	
	Questi sistemi a tre corpi possono essere caratterizzati da scale di massa e di lunghezza anche molto diverse tra loro, e sono comuni nell’Universo. Inoltre, spesso sono organizzati in una coppia interna in orbita ravvicinata e un terzo corpo che orbita a distanza più elevata. La tesi esplora allora la possibilità di studiare gli effetti perturbativi del corpo distante sull'orbita ravvicinata quasi-kepleriana dei primi due corpi.
	
	Seguendo quindi un'approccio perturbativo, possiamo studiare gli effetti del terzo corpo su un'ipotetica orbita perfettamente kepleriana della coppia interna. Descriviamo allora quest'orbita kepleriana con 5 coordinate ``lentamente" variabili (tipicamente semiasse maggiore $a$, eccentricità $e$, angoli di Eulero $\Omega, \iota, \omega$) e una coordinata fortemente dipendente dal tempo, solitamente l'anomalia vera o quella eccentrica. La descrizione del sistema con queste coordinate dà luogo alle equazioni cosiddette ``osculatrici".
	
	Le occorrenze naturali di questa situazione suggeriscono che in alcuni contesti si può in prima approssimazione trascurare la massa di uno dei due corpi in orbita ravvicinata. È questo il caso, ad esempio, di un sistema pianeta-satellite-stella, in cui la massa del satellite può essere spesso, in prima approssimazione, trascurata.
	
	Si può inoltre sviluppare al primo ordine la forza perturbante del terzo corpo (sviluppo noto come ``di quadrupolo").
	
	\section{Il meccanismo di Lidov-Kozai}
	\subsection{Analisi delle equazioni osculatrici}
	Si procede quindi usualmente valutando le variazioni temporali medie dei 5 parametri dell'orbita stretta, considerando come imperturbata l'orbita che coinvolge il terzo corpo e ipotizzando che i 5 parametri kepleriani varino trascurabilmente nel corso di un'orbita. Queste variazioni, cumulandosi, possono diventare apprezzabili per tempi sufficientemente lunghi; esse sono, infatti, note con l'appellativo di variazioni secolari. 
	
	Si ottiene quindi un sistema di equazioni differenziali che descrive l'evoluzione temporale dell'orbita del ``satellite". Un risultato notevole che è possibile dedurre da questo sistema è la conservazione della quantità $\cos(\iota)\sqrt{1-e^2}$.
	
	Scegliamo allora di descrivere le orbite con questa costante, con il semiasse maggiore, che pure è costante, con $\omega$ e con una quarta variabile linearmente indipendente, $\chi$.
	
	
	Quasi ovunque l'evoluzione temporale del sistema porta ad uno scambio tra eccentricità e inclinazione dell'orbita, dando luogo a quello che è appunto noto come meccanismo di Lidov-Kozai.
	
	\subsection{Possibili evoluzioni temporali}
	Più nel dettaglio, lo studio del sistema di equazioni differenziali indica dei comportamenti differenti in dipendenza del valore di $\chi$: finché $\chi>\frac35$, allora l'argomento del periapside continua a procedere in avanti, mentre se $\chi<\frac35$ allora può succedere che l'argomento del periapside preceda. In particolare, questa possibilità fa si che ci siano
	delle evoluzioni del sistema rotanti e ``libranti".
	
	\subsection{Risonanza di Kozai}
	Si può mostrare inoltre che esiste una soluzione stazionaria del sistema, con $\omega=\frac\pi2$ (o $\frac{3\pi}2$), e con $\chi=\frac35$. In un intorno di questa soluzione stazionaria si hanno evoluzioni libranti. In particolare, per piccole oscillazioni intorno all'equilibrio, possiamo espandere il sistema di equazioni differenziali e ottenere che la variazione di $\omega$ e di $\chi$ è armonica. Questa possibilità è nota come risonanza di Lidov-Kozai. In prima approssimazione il periodo di queste piccole oscillazioni non dipende quindi dalla loro ampiezza, ma solo dai parametri di massa, distanza, angoli ed eccentricità di equilibrio. 
	
	Giusto per rendere l'idea degli ordini di grandezza, il perido di Kozai per Tritone, satellite maggiore di Nettuno, è dell'ordine di centinaia di migliaia di anni, per un satellite in orbita geostazionaria è dell'ordine del centinaio di anni, mentre per la Luna è dell'ordine di qualche anno.
	
	\section{Casi più generali}
	\subsection{Sviluppo di ottupolo, non test-particle e quadrupolo gravitazionale}
	È possibile estendere i risultati ottenuti in diverse direzioni. In primo luogo si può analizzare il termine successivo nello sviluppo della forza, detto di ``ottupolo'', che introduce ulteriori risonanze e comportamenti caotici. Inoltre, un satellite con massa non trascurabile fa sì che anche la orbita interna abbia un proprio momento angolare, che può essere scambiato con l'orbita che coinvolge il terzo corpo. Viene così meno l'ipotesi che questo terzo corpo si trovi in un orbita immutabile.
	
	Infine, tipicamente il meccanismo di Kozai può essere ``spento" anche dal fatto che il campo gravitazionale del ``pianeta" non sia sfericamente simmetrico, come nel caso della presenza di un momento di quadrupolo. In particolare, considerando sia la perturbazione del terzo corpo al primo ordine, che il termine dovuto al quadrupolo planetario, possiamo ricavare analiticamente il sistema di equazioni differenziali per le variazioni secolari. Da questo si ottiene che l'inverso del tempo caratteristico di azione della forza dovuta al quadrupolo scala come $J_2\frac{\xi^2}{p^2}$, dove $J_2$, così definito, è un quadrupolo normalizzato, per un corpo simmetrico rispetto all'asse $z$. D'altro canto sappiamo che l'inverso del tempo caratteristico del meccanismo di Kozai scala come $\frac{m p^3}{M R^3}$.
	
	Già un ragionamento qualitativo suggerisce che il confronto di questi due termini possa indicare il comportamento del sistema. In particolare ci aspettiamo che il meccanismo di Kozai sia predominante quando la seconda espressione ha un valore molto maggiore della prima e viceversa.
	
	Voglio ora proporre un caso numerico per mostrare questi due limiti. Ho scelto per questo di prendere i numeri in gioco nel caso di un ipotetico satellite di Urano, con un orbita lievemente inclinata di 5° rispetto all'equatore planetario. Nel caso di Urano, il valore limite per $J_2$ è dell'ordine di $10^{-6}$: un valore molto minore implicherebbe la predominanza del meccanismo di Lidov-Kozai. Proprio nel caso di Urano, a causa della grande inclinazione tra equatore planetario e eclittica, si giungerebbe nell'arco di qualche centinaia di migliaia di anni ad avere valori di eccentricità prossimi a 1, portando facilmente il satellite sotto il limite di Roche, distruggendolo. Nel caso opposto, con un valore di $J_2$ molto superiore a $10^{-6}$ l'orbita viene stabilizzata e si innesca il meccanismo della precessione nodale, dovuto alla presenza del quadrupolo planetario.
	
	Dal punto di vista osservativo registriamo dei satelliti di Urano in orbita quasi equatoriale e possiamo ritenere la loro orbita ragionevolmente stabile dato che $J_2$ è dell'ordine di $10^{-3}$, molto maggiore del valore critico di $10^{-6}$.
	
\end{document}
