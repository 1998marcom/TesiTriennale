#!/usr/bin/env python3
from numpy import *
import matplotlib.pyplot as plt
import sys
import integrals as gr
from scipy.integrate import solve_ivp

def result(ee):
	return -3*ee*pi

def func(xx, e):
	return cos(xx)**1*sin(xx)**0/(1+e*cos(xx))**3*pow(1-e**2,2.5)

def integral(Nxx, e):
	xx = linspace(0, 2*pi, Nxx)
	yy = 2*pi/Nxx*func(xx, e)
	return sum(yy)

Nxx = 10000
Nee = 2000
top = 0.9999

ee = linspace(0,top,Nee)
IntArray = []
for e in ee:
	IntArray.append(integral(Nxx, e))
IntArray = array(IntArray)

plt.plot(ee,IntArray, label='numerical')
plt.plot(ee,result(ee), label='result')
plt.legend()
plt.show()
