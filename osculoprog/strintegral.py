#!/usr/bin/env python3
from numpy import *
from time import time
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import sys

def func(xx, e):
	return cos(xx)**2*sin(xx)**2/(1+e*cos(xx))**4*pow(1-e**2,2.5)

def integral(Nxx, e):
	xx = linspace(0, 2*pi, Nxx)
	yy = 1/Nxx*func(xx, e)
	return sum(yy)

Nxx = int(sys.argv[1])
Nee = int(sys.argv[2])
top = float(sys.argv[3])

ee = linspace(0,top,Nee)
IntArray = []
for e in ee:
	IntArray.append(integral(Nxx, e))
IntArray = array(IntArray)

def fit(ee, a, c, f):
	return a+c*ee**2+f*ee**6

popt, pcov = curve_fit(fit, ee, IntArray)

print(popt)

plt.plot(ee,IntArray)
plt.plot(ee,fit(ee, *popt))
plt.show()
