from numpy import *
from scipy.optimize import curve_fit

def A(e):
	return pi*(1+4*e**2)*pow((1.0-e**2),-3.5)
def B(e):
	return pi*pow((1.0-e**2),-2.5)
def C(e):
	return pow((1.0-e**2),-1.5)*(pc[0]*e+pc[1]*e**2+pc[2]*e**3+pc[3]*e**4+pc[4]*e**5)
def D(e):
	return -e*pi*pow((1.0-e**2),-3.5)*(3+2*e**2)
def E(e):
	return -1*pi*e*pow((1.0-e**2),-2.5)
def F(e):
	return -e*pi*pow((1.0-e**2),-3.5)*(4+e**2)
def G(e):
	return pow((1.0-e**2),-2.5)*(pg[0]+pg[1]*e**2+pg[2]*e**4+pg[3]*e**6)
def H(e):
	return pow((1.0-e**2),-3.5)*(ph[0]+ph[1]*e**2+ph[2]*e**4)
def I(e):
	return -e*pi*pow((1.0-e**2),-2.5)*3
def L(e):
	return pow((1.0-e**2),-2.5)*(pl[0]*e+pl[1]*e**3+pl[2]*e**5)

def Cfit(e, a, b, c, d, f):
	return pow((1.0-e**2),-1.5)*(a*e+b*e**2+c*e**3+d*e**4+f*e**5)
def Gfit(e, a, b, c, d):
	return pow((1.0-e**2),-2.5)*(a+b*e**2+c*e**4+d*e**6)
def Hfit(e, a, b, c):
	return pow((1.0-e**2),-3.5)*(a+b*e**2+c*e**4)
def Lfit(e, a, b, c):
	return pow((1.0-e**2),-2.5)*(a*e+b*e**3+c*e**5)

def Cint(xx, e):
	return pow((1.0-e**2),+1.5)*cos(xx)**1*sin(xx)**2/(1+e*cos(xx))**3
def Gint(xx, e):
	return pow((1.0-e**2),+2.5)*cos(xx)**2*sin(xx)**2/(1+e*cos(xx))**4
def Hint(xx, e):
	return pow((1.0-e**2),+3.5)*cos(xx)**4*sin(xx)**0/(1+e*cos(xx))**4
def Lint(xx, e):
	return pow((1.0-e**2),+2.5)*cos(xx)**3*sin(xx)**0/(1+e*cos(xx))**3

Nxx = 2000
Nee = 1000
top=0.999

def integral(Nxx, ecc, func):
	xx = linspace(0, 2*pi, Nxx)
	yy = 2*pi/Nxx*func(xx, ecc)
	return sum(yy)
	
ee = linspace(0,top,Nee)

IntArray = []
for ecc in ee:
	IntArray.append(integral(Nxx, ecc, Cint))
IntArray = array(IntArray)
pc, pcov = curve_fit(Cfit, ee, IntArray)

IntArray = []
for ecc in ee:
	IntArray.append(integral(Nxx, ecc, Gint))
IntArray = array(IntArray)
pg, pcov = curve_fit(Gfit, ee, IntArray)

IntArray = []
for ecc in ee:
	IntArray.append(integral(Nxx, ecc, Hint))
IntArray = array(IntArray)
ph, pcov = curve_fit(Hfit, ee, IntArray)

IntArray = []
for ecc in ee:
	IntArray.append(integral(Nxx, ecc, Lint))
IntArray = array(IntArray)
pl, pcov = curve_fit(Lfit, ee, IntArray)
