#!/usr/bin/env python3
from numpy import *
import matplotlib.pyplot as plt
import sys
import integrals as gr
from scipy.integrate import solve_ivp

#seterr(all='raise')
I = 82/180*pi
G = 6.67e-11
M = 8.68e25
m = 1.99e30
R = 19 *150e9
xi = 25e6
J2 = 1e-7

def xfunc(p, e, i, W, w):
	result = [	cos(W)*cos(w)-cos(i)*sin(W)*sin(w) ,
				cos(w)*sin(W)+cos(W)*cos(i)*sin(w) ,
				sin(i)*sin(w) ]
	return result
def yfunc(p, e, i, W, w):
	result = [	-cos(W)*sin(w)-cos(i)*sin(W)*cos(w) ,
				cos(W)*cos(w)*cos(i)-sin(W)*sin(w) ,
				sin(i)*cos(w) ]
	return result
def zfunc(p, e, i, W, w):
	result = [	sin(W)*sin(i) ,
				-cos(W)*sin(i) ,
				cos(i) ]
	return result
def xNxNfunc(p, e, i, W, w):
	x = xfunc(p, e, i, W, w)
	return 0.5*(x[0]**2+x[1]**2*cos(I)**2+x[2]**2*sin(I)**2+2*x[1]*x[2]*sin(I)*cos(I))
def yNyNfunc(p, e, i, W, w):
	y = yfunc(p, e, i, W, w)
	return 0.5*(y[0]**2+y[1]**2*cos(I)**2+y[2]**2*sin(I)**2+2*y[1]*y[2]*sin(I)*cos(I))
def xNyNfunc(p, e, i, W, w):
	x = xfunc(p, e, i, W, w)
	y = yfunc(p, e, i, W, w)
	return 0.5*(x[0]*y[0]+x[1]*y[1]*cos(I)**2+x[2]*y[2]*sin(I)**2
		+(x[1]*y[2]+x[2]*y[1])*sin(I)*cos(I))
def zNxNfunc(p, e, i, W, w):
	x = xfunc(p, e, i, W, w)
	z = zfunc(p, e, i, W, w)
	return 0.5*(x[0]*z[0]+x[1]*z[1]*cos(I)**2+x[2]*z[2]*sin(I)**2
		+(x[1]*z[2]+x[2]*z[1])*sin(I)*cos(I))
def zNyNfunc(p, e, i, W, w):
	y = yfunc(p, e, i, W, w)
	z = zfunc(p, e, i, W, w)
	return 0.5*(z[0]*y[0]+z[1]*y[1]*cos(I)**2+z[2]*y[2]*sin(I)**2
		+(z[1]*y[2]+z[2]*y[1])*sin(I)*cos(I))
def Pfunc(p, e, i, W, w):
	try:
		returno = 2*pi*sqrt(p**3/G/M/(1.0-e**2)**3)
	except:
		print(p, G, M, e)
		quit()
	return returno
p0 = 400e6
e0 = 0.05
i0 = 5*pi/180
W0 = 0
w0 = 1.57
init = [p0, e0, i0, W0, w0]

def system(t, y): #t is useless
	p, e, i, W, w = y[0], y[1], y[2], y[3], y[4]
	e=min(e,0.999999)
	P = Pfunc(p, e, i, W, w)
	A = gr.A(e); B = gr.B(e); C = gr.C(e); D = gr.D(e); E = gr.E(e)
	F = gr.F(e); G = gr.G(e); H = gr.H(e); I = gr.I(e); L = gr.L(e)
	xNxN = xNxNfunc(p, e, i, W, w); yNyN = yNyNfunc(p, e, i, W, w)
	xNyN = xNyNfunc(p, e, i, W, w); zNxN = zNxNfunc(p, e, i, W, w)
	zNyN = zNyNfunc(p, e, i, W, w)
	k = m*p**3/M/R**3/P
	pt = 6*k*p*xNyN*(A-B)
	#et = 3*k*xNyN*(2*C+2*D-2*E+e*A-e*B+e*H-e*G)
	et = 3*k*xNyN*(2*D+2*e*A-e*B)
	it = 3*k*(zNxN*cos(w)*A-zNyN*sin(w)*B)
	Wt = 3*k/sin(i)*(zNxN*sin(w)*A+zNyN*cos(w)*B) - 3*pi*J2/P*(xi/p)**2*cos(i)
	#wt = k/e*(I-3*xNxN*L-3*yNyN*C+6*E*(yNyN-xNxN)+3*e*G*(yNyN-xNxN)
	#	-3*e/tan(i)*(zNxN*sin(w)*A+zNyN*cos(w)*B)) + 6*pi*J2/P*(xi/p)**2*(1-5/4*sin(i)**2)
	wt = k/e*(I-3*xNxN*(D+e*A)+3*yNyN*E+6*E*(yNyN-xNxN)
		-3*e/tan(i)*(zNxN*sin(w)*A+zNyN*cos(w)*B)) + 6*pi*J2/P*(xi/p)**2*(1-5/4*sin(i)**2)
	return [pt, et, it, Wt, wt]
	
FinalT = 1.0e14
result = solve_ivp(system, (0.0, FinalT), init, max_step=FinalT/2000, method='Radau')
tt = result.t
yy = result.y
p, e, i, W, w = yy[0], yy[1], yy[2], yy[3], yy[4]

fig = plt.figure()
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212, sharex=ax1)

ax1.plot(tt/(365.24*24*3600*1e6), e, label='$e$')
ax1.plot(tt/(365.24*24*3600*1e6), i, label='$\iota$')

ax2.plot(tt/(365.24*24*3600*1e6), W, label='$\Omega$')
ax2.plot(tt/(365.24*24*3600*1e6), w, label='$\omega$')

cosItot = cos(I)*cos(i)+sin(I)*sin(i)*cos(W)
ax1.plot(tt/(365.24*24*3600*1e6), sqrt(1-e**2)*cosItot, label='$\sqrt{1-e^2}\cos(I_{tot})$')

plt.text(0.98, 0.02,'''
$p_0=400\\times10^{6}$m
$e_0= 0.05\;,\quad\iota_0=5°$
$\Omega_0=0°\;,\quad\omega_0=90°$
$J_2 = 1\\times10^{-7}$
''',
     horizontalalignment='right',
     verticalalignment='bottom',
     transform = plt.gca().transAxes)

ax1.legend(loc='upper left', fontsize=12)
ax1.set_ylabel('', fontsize=12)
ax1.set_xlabel('', fontsize=0)
ax1.tick_params(axis='x', which='major', labelsize=0, labelbottom=False)
ax1.tick_params(axis='y', which='major', labelsize=12)

ax2.legend(loc='upper left', fontsize=12)
ax2.set_ylabel('', fontsize=12)
ax2.set_xlabel('time [MYrs]', fontsize=12)
ax2.tick_params(axis='x', which='major', labelsize=12)
ax2.tick_params(axis='y', which='major', labelsize=12)

fig.set_size_inches(5, 3.5)
fig.tight_layout()
plt.savefig('../presentazione/uberkozai/J2-7.png', transparent=True, dpi=300)

plt.show()
