Nel contesto della gravità Newtoniana, un problema sovente oggetto di studio è quello dei tre
corpi. Spesso in natura questi sistemi sono organizzati in una coppia in orbita ravvicinata fra di
loro e un terzo corpo orbitante a distanza più elevata. Può quindi essere interessante studiare
gli effetti perturbativi del corpo distante sull’orbita stretta quasi-kepleriana dei primi due corpi.
Le occorrenze naturali di tale situazione suggeriscono che in alcuni contesti si può validamente
trascurare la massa di uno dei due corpi in orbita stretta. Si pensi, ad esempio, ad un sistema
stella-pianeta-satellite.
Si può quindi procedere a studiare perturbativamente gli effetti del terzo corpo su un’ipotetica
orbita kepleriana del ”satellite”. Descriviamo allora il suo moto con un set di 5 coordinate
”lentamente” variabili che definiscono un’orbita kepleriana (tipicamente semiasse maggiore a,
eccentricità e, angoli di Eulero Ω, ι, ω) e una coordinata fortemente dipendente dal tempo,
solitamente l’anomalia vera o quella eccentrica. La descrizione in termini di queste coordinate dà
luogo alle equazioni cosiddette ”osculatrici”. Si può inoltre sviluppare al primo ordine la forza
perturbante del terzo corpo (sviluppo noto come ”di quadrupolo”).
Si procede quindi usualmente valutando le variazioni temporali medie dei 5 parametri
dell’orbita stretta, considerando come immutabile l’orbita che coinvolge il terzo corpo e ipotizzando
che i 5 parametri kepleriani varino trascurabilmente nel corso di un’orbita. Queste variazioni,
cumulandosi, possono diventare apprezzabili per tempi sufficientemente lunghi; esse sono, infatti,
note con l’appellativo di variazioni secolari.
Si ottiene quindi un sistema di equazioni differenziali che descrive l’evoluzione temporale
dell’orbita del ”satellite”. Un risultato notevole che è possibile dedurre da questo sistema è la
conservazione della quantità. Inoltre, quasi ovunque l’evoluzione
temporale del sistema porta ad uno scambio tra e e ι, tenendo sempre costante, dando luogo a
quello che è appunto noto come meccanismo di Lidov-Kozai. Più nel dettaglio, lo studio del
sistema di equazioni differenziali indica che ci siano sia delle evoluzioni del sistema rotanti che delle evoluzioni
”libranti”. Nel caso di piccole oscillazioni, si può mostrare che esistono delle evoluzioni libranti
per cui la variazione di e, ι, ω è armonica. Il periodo di queste piccole oscillazioni non dipende
quindi dalla loro ampiezza, ma solo dai parametri di massa, distanza, angoli ed eccentricità di
equilibrio. Questa possibilità è nota come risonanza di Lidov-Kozai
Per valutare casi più generali si può procedere ad analizzare il termine successivo nello
sviluppo della forza, ovvero quello di ottupolo, che introduce ulteriori risonanze e comportamenti
caotici. Inoltre, una particella non massless può turbare il moto di , dando luogo a valori
estremali di e ed ι differenti. Infine, tipicamente il meccanismo di Kozai può essere ”spento”
anche dal fatto che il campo gravitazionale del ”pianeta” non sia sfericamente simmetrico, ma
sia ad esempio dotato di un momento di quadrupolo. È questo il motivo, ad esempio, per cui le
orbite dei satelliti di un pianeta come Urano siano stabili nonostante l’elevata inclinazione tra
queste e l’orbita del pianeta intorno al Sole.
