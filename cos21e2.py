#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from math import *

A=0.1
ic = 1
ec = sqrt(1-5/3*cos(ic)**2)

tt = np.linspace(0,10, 1000)


def i(tt):
	return ic-1/2*A*ec*np.cos(tt)
	
def e(tt):
	return ec+5/6*A*sin(ic)*cos(ic)*np.cos(tt)
	
plt.plot(tt, np.cos(i(tt))**2/(1-e(tt)**2))
plt.show()
