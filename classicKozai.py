#!/usr/bin/env python3
from numpy import *
import matplotlib.pyplot as plt
import sys
from scipy.integrate import solve_ivp

P = 13/365
e0 = 0.05
#e0 = 0.06854358438441834
i0 = 80*pi/180
#i0 = 0.6863149691083157
w0 = pi/2-0.01
#w0 = 1.6
param = 64.e-9

def system(t, y):
	e = y[0]; i = y[1]; w = y[2] 
	et = 1./P * 15*pi/2*param*e*sqrt(1-e**2)*sin(i)**2*sin(w)*cos(w)
	wt = 1./P * 3*pi/2*param/sqrt(1-e**2)*(5*cos(i)**2*sin(w)**2+(1-e**2)*(5*cos(w)**2-3))
	it = -15*pi/2/P * param*e**2/sqrt(1-e**2)*sin(i)*cos(i)*sin(w)*cos(w)
	return [et, it, wt]



init = [e0, i0, w0]

FinalT = 2.0e6
result = solve_ivp(system, (0.0, FinalT), init, max_step=FinalT/1000, method='Radau')
tt = result.t
yy = result.y
e, i, w = yy[0], yy[1], yy[2]

plt.plot(tt, e, label='eccentricity')
plt.plot(tt, i, label='inclination')
plt.plot(tt, w, label='omega')
plt.plot(tt, sqrt(1-e**2)*cos(i), label='should be const')
plt.plot(tt, cos(i)**2/(1-e**2), label='cos(Itot)**2/(1-e**2)')
plt.xlabel('time [Yrs]')
#plt.ylim(-4,+4)
plt.legend()
plt.show()
index = int(1000000/FinalT*len(e))
print(e[index], i[index], w[index])
