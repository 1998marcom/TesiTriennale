#!/usr/bin/env python3
from numpy import *
import matplotlib.pyplot as plt
import sys
from scipy.integrate import solve_ivp

Jz = 0.6
pc = 0.4

e0 = sqrt(1-sqrt(Jz**2/pc))
i0 = arccos(pow(Jz**2*pc,0.25))
w0 = 90*pi/180


def system(t, y):
	e = y[0]; i = y[1]; w = y[2] 
	et = 1. * 15*pi/2*e*sqrt(1-e**2)*sin(i)**2*sin(w)*cos(w)
	wt = 1. * 3*pi/2/sqrt(1-e**2)*(5*cos(i)**2*sin(w)**2+(1-e**2)*(5*cos(w)**2-3))
	it = -15*pi/2 *e**2/sqrt(1-e**2)*sin(i)*cos(i)*sin(w)*cos(w)
	return [et, it, wt]


init = [e0, i0, w0]

FinalT = 3
result = solve_ivp(system, (0.0, FinalT), init, max_step=FinalT/2000, method='Radau')
tt = result.t
yy = result.y
e, i, w = yy[0], yy[1], yy[2]

fig=plt.figure()

ax1 = fig.add_subplot(211)
ax1.plot(tt, e, label='e')
ax1.plot(tt, i, label='$\iota$')
#ax1.plot(tt, sqrt(1-e**2)*cos(i), label='$\sqrt{1-e^2}\cos(\iota)$')
ax1.plot(tt, cos(i)**2/(1-e**2), label='$\\frac{\cos^2(\iota)}{1-e^2}$')
#plt.ylim(-4,+4)
ax1.legend(loc='upper left', fontsize=12)

#ax1.spines['top'].set_color('none')
#ax1.spines['bottom'].set_color('none')
#ax1.spines['left'].set_color('none')
#ax1.spines['right'].set_color('none')
ax1.tick_params(labelcolor='k', top=False, bottom=True, left=True, right=False, labelbottom=False)
ax1.tick_params(axis='y', which='major', labelsize=12)
ax1.tick_params(axis='x', which='major', labelsize=0)

ax2 = fig.add_subplot(212, sharex=ax1)
ax2.plot(tt, w, label='$\omega$')
ax2.legend(loc='upper left', fontsize=12)
ax2.set_xlabel('time [$P/\lambda$]', fontsize=12)
ax2.tick_params(axis='x', which='major', labelsize=12)
ax2.tick_params(axis='y', which='major', labelsize=12)

fig.set_size_inches(4, 4)
plt.text(0.97, 0.02,'''
$e_0$ = {:.2f}
$\iota_0$ = {:.2f} rad
$\omega_0$ = {:.2f} rad
'''.format(e0, i0, w0),
     horizontalalignment='right',
     verticalalignment='bottom',
     fontsize=12,
     transform = plt.gca().transAxes)

fig.tight_layout()
plt.savefig('presentazione/kozai/oldKozai-3tmp.png', transparent=True)
plt.show()
