# Tesi Triennale

![build status](https://gitlab.com/1998marcom/TesiTriennale/badges/master/pipeline.svg)

Repo per la tesi triennale.

Riassunto disponibile [qui](https://gitlab.com/1998marcom/TesiTriennale/-/jobs/artifacts/master/raw/riassunto.pdf?job=build).

Presentazione disponibile [qui](https://gitlab.com/1998marcom/TesiTriennale/-/jobs/artifacts/master/raw/presentazione.pdf?job=build).

Copione disponibile [qui](https://gitlab.com/1998marcom/TesiTriennale/-/jobs/artifacts/master/raw/copione.pdf?job=build).

![CC-BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png "CC-BY-SA")
(salvo ove espressamente diversamente indicato)

# Schema indicativo della tesi

### Intro: problema a 2 vs 3 corpi
 * Inner couple + oggetto perturbante
 * Eq. secolari
 * Approssimazione gerarchica
 * Sviluppo di "quadrupolo" sulla forza

### Il meccanismo di Kozai

###### Analisi delle equazioni osculatrici
 * cos(i) * sqrt(1-e^2) = cost
 * caso cos^2(i)/(1-e^2) < 3/5
 * caso cos^2(i)/(1-e^2) > 3/5
###### Risonanza di Kozai
 * equilibrio stabile con oscillazioni
 * legge oraria delle oscillazioni e "periodo di Kozai"
### Oltre Kozai
###### Ottupolo
 * qualitativo
###### Non-test particle
 * qualitativo
###### Inner body real quadrupole
 * quantitative solution through numerical integration of secular equations
 * script reference
 * J2 definition (formal & intuitive)
 * Analytical deduction of border condition
 * Show some simulation results that confirm hypothesis
 * Real world-case: Uranus numbers
